/*
	CLIENT-SERVER ARCHITECTURE

	Client-server architecture is a computing model wherein a server hosts, delivers and manages resources that a client consumes.




	What is a client?

	A client is an application which creates requests for resources from a server. A client will trigger an action, in a wev development context, through a URL and wait for the response of the server.
	



	What is a Server?

	A server is able to host and deliver resources requested by a client. In fact, a single server can handle multiple clients.




	What is Nodejs?

	Node.js is an environment to be able to develop applications with Javascript. With this, we can run JS even without the use of an HTML.

	Javascript was originally conceptualized to be used for Front-End applications. This is the reason why our vanilla Javascript had to be linked and connected to an HTML file before you can run the JS.

	But with advent of Node.js, JS can now be used to create backend applications.

		Note: 
			FRONT-END is usually the page that we're seeing.
			It is usually a client application that requests resources from a server or a backend.

			BACK-END applications are usually server-related applications which handles requests from a Front-End or a CLIENT.


	



	METHOD - are functions in an object






	Why is Node.js so popular?

	-Popularity
	-Familiarity
	-Funcionality

*/


// console.log("Hello Wordl!");


/*
	


	We used require() method to load node.js modules.

		A module is a software component or part of a program which contains one or more routines.

		The http module is a default from node.js


		The HTTP(hypertext transfer protocol) module let node.js transfer data or let our client and server exchange data via HTTP.

		protocol => http://localhost:4000 <= server

		This is how clients and servers communicate with each other. A client triggers an action from a server via URL and the server responds with the data.

			Note:
				Messages from a client which triggers an action from a server is called a REQUEST.

				Messages from a server to respond to a client's request is called a RESPONSE.
		
*/




let http = require("http");

	
http.createServer(function(req,res){

	if(req.url === "/"){

	res.writeHead(200,{'Content-Type':'text/plain'});
	res.end('Hi! My name is Aaron Keane')
	} else if (req.url === "/login"){
		res.writeHead(200,{'Content-Type': 'text/plain'});
		res.end('Welcome to the Login Page')
	}

}).listen(4000);

console.log("Server is running on localHost:4000")
